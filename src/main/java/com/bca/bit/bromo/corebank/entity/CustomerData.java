package com.bca.bit.bromo.corebank.entity;

import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name="ms_customer")
public class CustomerData {
	
	@Id
	@Column(name = "cis_no")
	private UUID cisNo;
	
	@Column(name = "first_name")
	private String firstName;
	
	@Column(name = "last_name")
	private String lastName;
	
	@Column(name = "date_of_birth")
	private Date dob;
	
	@Column(name = "place_of_birth")
	private String placeOfBirth;
	
	@Column(name = "gender")
	private String gender;
	
	@Column(name = "email")
	private String email;
	
	@Column(name = "phone_no")
	private String phoneNo;
	
	@Column(name = "address")
	private String address;
	
	@Column(name = "id_no")
	private String idNo;
	
	@Column(name = "job")
	private String job;

}
