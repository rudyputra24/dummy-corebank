package com.bca.bit.bromo.corebank.entity.request;

import lombok.Data;

@Data
public class DebitRequest {
	
	private String card;
	private String virtualAccount;
	private int amountDebit;

}
