package com.bca.bit.bromo.corebank.repo;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.bca.bit.bromo.corebank.entity.CardData;

@Repository
public interface CardDataRepo extends PagingAndSortingRepository<CardData, String>{

	CardData findByCredNo(String cardNo);
	
}
