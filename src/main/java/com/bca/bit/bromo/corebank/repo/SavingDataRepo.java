package com.bca.bit.bromo.corebank.repo;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.bca.bit.bromo.corebank.entity.SavingData;

public interface SavingDataRepo extends PagingAndSortingRepository<SavingData, String> {
	
	SavingData findByAccountNo(String accountNo);
	

}
