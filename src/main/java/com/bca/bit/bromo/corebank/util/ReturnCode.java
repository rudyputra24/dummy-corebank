package com.bca.bit.bromo.corebank.util;

import lombok.Data;

@Data
public class ReturnCode {
    public static final String COREBANK_SERVICE_SUCCESS = "BIT-200";
    public static final String COREBANK_BALANCE_NOT_ENOUGH = "BIT-401";
    public static final String COREBANK_CRED_NOT_ACTIVE = "BIT-402";
    public static final String COREBANK_NO_ACCOUNT = "BIT-403";
    public static final String COREBANK_DATA_NOT_FOUND = "BIT-404";
    public static final String COREBANK_SERVICE_GENERAL_ERROR = "BIT-499";
}
