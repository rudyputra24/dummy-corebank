package com.bca.bit.bromo.corebank.util;

import lombok.Data;

import java.util.HashMap;

@Data
public class MessageMapIndonesian {
    private static HashMap<String, String> messageMap;
    public static final String LANGUAGE_CODE = "ID";

    static {
        messageMap = new HashMap<String, String>();
        messageMap.put(ReturnCode.COREBANK_SERVICE_SUCCESS, "Berhasil");
        messageMap.put(ReturnCode.COREBANK_DATA_NOT_FOUND, "Data tidak ditemukan");
        messageMap.put(ReturnCode.COREBANK_SERVICE_GENERAL_ERROR, "Terjadi kesalahan dalam proses");
        messageMap.put(ReturnCode.COREBANK_CRED_NOT_ACTIVE, "Kartu tidak aktif");
        messageMap.put(ReturnCode.COREBANK_BALANCE_NOT_ENOUGH, "Saldo tidak cukup");
        messageMap.put(ReturnCode.COREBANK_NO_ACCOUNT, "Tidak ada akun yang bisa didebet");
    }

    public static String getMessage(String messageCode){return messageMap.get(messageCode);}
}
