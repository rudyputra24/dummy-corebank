package com.bca.bit.bromo.corebank.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bca.bit.bromo.corebank.entity.request.DebitRequest;
import com.bca.bit.bromo.corebank.entity.response.DebitBalanceResponse;
import com.bca.bit.bromo.corebank.entity.response.InquiryCardResponse;
import com.bca.bit.bromo.corebank.service.CorebankService;

@RestController
@RequestMapping("/corebank/inquiry")
public class CorebankController {
	
	@Autowired
	private CorebankService corebankSvc;

	@GetMapping("/card")
	public InquiryCardResponse inquiry(@RequestParam String card){
		InquiryCardResponse resp = new InquiryCardResponse();
		resp = corebankSvc.inquiryCard(card);
		return resp;
	}
	
	@PutMapping("/balance")
	public DebitBalanceResponse debitBalance(@RequestBody DebitRequest debitRequest) {
		DebitBalanceResponse resp = new DebitBalanceResponse();
		resp = corebankSvc.debitAccount(debitRequest.getCard(),debitRequest.getAmountDebit(),debitRequest.getVirtualAccount());
		return resp;
		
	}
	
}
