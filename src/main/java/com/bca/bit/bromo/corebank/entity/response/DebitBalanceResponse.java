package com.bca.bit.bromo.corebank.entity.response;

import com.bca.bit.bromo.corebank.util.BaseResponse;
import lombok.Data;

@Data
public class DebitBalanceResponse extends BaseResponse{

	public DebitBalanceResponse() {
		
	}
	
	public DebitBalanceResponse(String status, String errorCode) {
		super(errorCode);
		this.outputSchema = new OutputSchema(status);
	}
	
	private OutputSchema outputSchema;

	@Data
	public static class OutputSchema {
		private String status;
		
		public OutputSchema(String status) {
			this.status = status;
		}
	}
}
