package com.bca.bit.bromo.corebank.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name="ms_card_data")
public class CardData {

	@Id
	@Column(name="cred_no")
	private String credNo;
	
	@ManyToOne
	@JoinColumn(name="cis_no")
	private CustomerData customer;
	
	
	@Column(name="status")
	private String status;
	
	@OneToMany
	@JoinColumn(name="cred_no")
	private List<SavingData> listSaving; 
	
}
