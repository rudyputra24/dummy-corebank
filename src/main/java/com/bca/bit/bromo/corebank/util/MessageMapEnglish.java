package com.bca.bit.bromo.corebank.util;

import lombok.Data;

import java.util.HashMap;

@Data
public class MessageMapEnglish {
    private static HashMap<String, String> messageMap;
    public static final String LANGUAGE_CODE = "EN";

    static {
        messageMap = new HashMap<String, String>();
        messageMap.put(ReturnCode.COREBANK_SERVICE_SUCCESS, "Success");
        messageMap.put(ReturnCode.COREBANK_DATA_NOT_FOUND, "Data not found");
        messageMap.put(ReturnCode.COREBANK_SERVICE_GENERAL_ERROR, "Error in processing service");
        messageMap.put(ReturnCode.COREBANK_CRED_NOT_ACTIVE, "Account not active");
        messageMap.put(ReturnCode.COREBANK_BALANCE_NOT_ENOUGH, "Balance not enough");
        messageMap.put(ReturnCode.COREBANK_NO_ACCOUNT, "No Account able to process");
    }

    public static String getMessage(String messageCode){return messageMap.get(messageCode);}
}
