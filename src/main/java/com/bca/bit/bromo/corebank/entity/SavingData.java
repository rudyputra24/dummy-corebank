package com.bca.bit.bromo.corebank.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name="ms_savings")
public class SavingData {

	@Id
	@Column(name = "account_no")
	private String accountNo;
	
	@Column(name = "balance")
	private int balance;
	
	@Column(name = "cred_no")
	private String cred_no;
	
	@Column(name = "status")
	private String status;
	
}
