package com.bca.bit.bromo.corebank.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bca.bit.bromo.corebank.entity.CardData;
import com.bca.bit.bromo.corebank.entity.SavingData;
import com.bca.bit.bromo.corebank.entity.response.DebitBalanceResponse;
import com.bca.bit.bromo.corebank.entity.response.InquiryCardResponse;
import com.bca.bit.bromo.corebank.entity.response.InquiryCardResponse.ErrorSchema;
import com.bca.bit.bromo.corebank.repo.CardDataRepo;
import com.bca.bit.bromo.corebank.repo.SavingDataRepo;
import com.bca.bit.bromo.corebank.util.ReturnCode;

@Service
public class CorebankService {

	@Autowired
	private CardDataRepo cardDataRepo;

	@Autowired
	private SavingDataRepo savingDataRepo;

	public List<CardData> inquiry() {
		List<CardData> listCard = (List<CardData>) cardDataRepo.findAll();
		return listCard;
	}

	public InquiryCardResponse inquiryCard(String card) {
		InquiryCardResponse resp = new InquiryCardResponse();
		InquiryCardResponse.ErrorSchema errSchema = new InquiryCardResponse.ErrorSchema();
		InquiryCardResponse.ErrorSchema.ErrorMessage errMsg = new InquiryCardResponse.ErrorSchema.ErrorMessage();
		InquiryCardResponse.OutputSchema outputSchema = new InquiryCardResponse.OutputSchema();

		boolean flagSaving = false;

		String accountNumber = "";

		try {

			CardData cardData = cardDataRepo.findByCredNo(card);
			System.out.println("Card is " + card);
			if (cardData == null || card == null || card.equals("")) {
				errSchema.setErrorCode("BIT-404");
				errMsg.setEnglish("Data not found");
				errMsg.setIndonesian("Data tidak ditemukan");
			} else if (cardData.getStatus().equals("active")) {
				for (SavingData sd : cardData.getListSaving()) {
					if (sd.getStatus().equals("active")) {
						flagSaving = true;
						accountNumber = sd.getAccountNo();
						break;
					}
				}
				if (flagSaving == true) {
					errSchema.setErrorCode("BIT-200");
					errMsg.setEnglish("Success");
					errMsg.setIndonesian("Sukses");

					outputSchema.setGender(cardData.getCustomer().getGender());
					outputSchema.setPhoneNo(cardData.getCustomer().getPhoneNo());
					outputSchema.setName(
							cardData.getCustomer().getFirstName() + " " + cardData.getCustomer().getLastName());
					outputSchema.setCardNo(card);
					outputSchema.setAccountNo(accountNumber);
					outputSchema.setStatus("Allowed");
				} else {
					errSchema.setErrorCode("BIT-200");
					errMsg.setEnglish("Success");
					errMsg.setIndonesian("Sukses");

					outputSchema.setCardNo(card);
					outputSchema.setStatus("Not Allowed");
				}

			} else {
				errSchema.setErrorCode("BIT-200");
				errMsg.setEnglish("Success");
				errMsg.setIndonesian("Sukses");
			}
		} catch (Exception e) {
			errSchema.setErrorCode("BIT-500");
			errMsg.setEnglish("Internal Server Error");
			errMsg.setIndonesian("Internal Server Error");
		}

		errSchema.setErrorMessage(errMsg);
		resp.setOutputSchema(outputSchema);
		resp.setErrorSchema(errSchema);

		return resp;
	}

	public DebitBalanceResponse debitAccount(String card, int amount, String vaAcount) {
		DebitBalanceResponse debitBalanceResponse;

		CardData cardData = cardDataRepo.findByCredNo(card);

		try {

			if (cardData.getStatus().equals("active")) {
				SavingData saving = cardData.getListSaving().get(0);

				if (saving.getStatus().equals("active")) {
					if (saving.getBalance() < amount) {
						debitBalanceResponse = new DebitBalanceResponse("Failed",
								ReturnCode.COREBANK_BALANCE_NOT_ENOUGH);
					} else {
						int balanceUpdated = saving.getBalance() - amount;
						saving.setBalance(balanceUpdated);
						savingDataRepo.save(saving);
						debitBalanceResponse = new DebitBalanceResponse("Success", ReturnCode.COREBANK_SERVICE_SUCCESS);
					}
				} else {
					debitBalanceResponse = new DebitBalanceResponse("Failed", ReturnCode.COREBANK_NO_ACCOUNT);
				}
			} else {
				debitBalanceResponse = new DebitBalanceResponse("Failed", ReturnCode.COREBANK_CRED_NOT_ACTIVE);
			}

		} catch (Exception e) {
			debitBalanceResponse = new DebitBalanceResponse("Failed", ReturnCode.COREBANK_SERVICE_GENERAL_ERROR);
			e.printStackTrace();
		}

		return debitBalanceResponse;
	}

}
