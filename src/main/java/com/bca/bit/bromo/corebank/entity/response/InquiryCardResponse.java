package com.bca.bit.bromo.corebank.entity.response;

import lombok.Data;

@Data
public class InquiryCardResponse {

	private ErrorSchema errorSchema;
	private OutputSchema outputSchema;

	@Data
	public static class ErrorSchema {
		private String errorCode;
		private ErrorMessage errorMessage;

		@Data
		public static class ErrorMessage {
			private String english;
			private String indonesian;
		}
	}

	@Data
	public static class OutputSchema {
		private String cardNo;
		private String name;
		private String gender;
		private String phoneNo;
		private String status;
		private String accountNo;
	}
}
